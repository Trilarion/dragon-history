block _0 ( beg(1)
title
{disablespeedtext
disablequickhero
stayon -24 170 vpravo

start hulka "kamen_mluvi"
   EvIn: "Bed� rz�dzi� �wiatem. Lecz jak to si� robi?"
walkon 75 175 vpravo
   EvIn: "Kto to?"
start hulka "na kameni"
   EvIn: "Mam go. Moim s�ug� b�dzie w�a�nie on,
          m�ody, naiwny, niedo�wiadczony smok!"
   EvIn: "Hej, st�j!"
startplay hulka "premet"
start hulka "mluvi"
justtalk
   D: "Czekam na ciebie..."
juststay
   EvIn: "Zadam ci kilka pyta�."
   EvIn: "Czy chcesz sprawowa� w�adz� nad �wiatem?"
justtalk
   D: "A czy wiesz jak tego dokona�?"
juststay
start hulka "mluvi"

   EvIn: "Jasne, �e wiem.
          Patrz, mam ber�o."
startplay hulka "placka_vznik"
start hulka "placka_mlci"

justtalk
   D: "Je-stem ju� (prawie) w�adc� �wiata.
       Spytaj mnie, jak te-go do-kona�am!"
   D: "Hmm, to brzmi interesuj�co.
       Dzi� po po�udniu nie mam nic do roboty."
   D: "Zacznijmy od zaraz."
juststay

start hulka "placka_mluvi"
   EvIn: "Nie zaszkodzi naby� nieco wprawy
          w rz�dzeniu �wiatem."
   EvIn: "Co ty na to, by podzia�a� komu� na nerwy?"
start hulka "placka_mlci"
justtalk
   D: "Dobry pomys�, mo�na by to zrobi� ju� dzi�
       po po�udniu."
juststay

gplend
