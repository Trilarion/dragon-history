{loutk��}

block _03 ( beg(1) and not been(_03) and isactico(0) )
title
  {dokud nep�edal d�ev�n� loutky:}
  justtalk
   D: "Panie Artysto!"
  juststay
  start kom "mluvi"
   U: "Co to za maniery! Co ty tu robisz?"
  start kom "mlci"
  justtalk
   D: "Przyszed�em tu, by wypr�bowa�
       czarodziejskie zakl�cia."
   D: "Czy przypadkiem nie �yczysz sobie,
       abym zamieni� co� w kamie�?"
  juststay
  start kom "mluvi"
   U: "Id� sobie!"
  start kom "mlci"
  justtalk
   D: "(Nie znam jeszcze tego zakl�cia,
       musz� si� du�o nauczy�)"
  juststay
  exitdialogue
gplend

block _05 ( isactico(0) and maxline(1) and beg(1) and not been(_05) and been(_03) )
title
  justtalk
   D: "Panie Artysto!"
   D: "Przyszed�em, by odczarowa�
       twoje marionetki,
       �eby zn�w by�y drewniane."
  juststay
  start kom "mluvi"
   U: "Co? Co powiedzia�e� na ko�cu?"
  start kom "mlci"
  justtalk
   D: "�eby twoje marionetki zn�w by�y drewniane!"
  juststay
  start kom "mluvi"
   U: "Je�li chcesz moje kamienne laleczki
       to mo�esz je sobie wzi��!"
   U: "Na przyciski do papier�w. Nie s� mi potrzebne.
       A teraz, id� precz!"
  start kom "mlci"
  justtalk
   D: "Jako przyciski do papieru
       nie wygl�da�yby zbyt gustownie.
       Nadawa�y by si� raczej
       do skalnego ogr�dka mojej matki."
  juststay
  start kom "mluvi"
   U: "Cicho, nie denerwuj mnie!"
  start kom "mlci"
  {dostane loutky}
  icostat on i_loutka_kamenna
gplend

block _00 ( maxline(1) and beg(1) and (vim_o_kronice=1) and not been(_00) and been(_04) and isactico(0) )
title
  {a� mi ��kal jazykolamy a zkou�el jsem mluvit s kronikou
  justtalk
   D: "Co robisz, �e masz tak doskona�� dykcj�?"
  juststay
  start kom "mluvi"
   U: "To nic trudnego.
       Trzeba du�o �wiczy�."
  start kom "mlci"
  justtalk
   D: "Ale same �wiczenia nie wystarcz�, prawda?"
  juststay
  start kom "mluvi"
   U: "Masz racj�. Ju� si� nie gniewam,
       wi�c mog� ci to zdradzi�.
       Korzystam z mocy z/io�."
  start kom "mlci"
  justtalk
   D: "Czy parzysz z nich herbatk�?"
  juststay
  start kom "mluvi"
   U: "�uj� kwiaty."
  load kom "zvyka"
  startplay kom "zvyka"
  start kom "mlci"

  justtalk
   D: "Gdzie mo�esz znale�� takie ziele?"
  juststay
  start kom "mluvi"
   U: "Wysoko, wysoko w g�rach."
  start kom "mlci"

  {udela se nova lokace na mape:}
  let new_hory (1)
gplend

block _01 ( maxline(1) and beg(1) and not been(_01) and isicoon(i_scenar) )
title
 labels skip
  {a� do�te poh�dku- hele, v kn��ce byly vlo�en� n�jak� listy,
  {prozkoum�m listy- 'siesta u �eky, hra o t�ech....
  {m��u za�st hovor o divadle:
  justtalk
   D: "Mam pytanie zwi�zane ze sztuk� dramatyczn�."
  juststay
  start kom "mluvi"
   U: "Prosz� bardzo, jestem ekspertem
       w tej dziedzinie."
  start kom "mlci"
  justtalk
   D: "Czy znasz sztuk� 'Sjesta nad brzegiem rzeki'?"
  juststay
  start kom "mluvi"
   U: "'Sjesta nad brzegiem rzeki'
       Sztuka w trzech aktach z epilogiem."
  start kom "mlci"
  justtalk
   D: "A wi�c znasz j�!"
  juststay
  start kom "mluvi"
   U: "Znam tylko kawa�ek tej sztuki, nigdy nie
       mia�em w r�ku ca�ego tekstu."
   U: "Ale te fragmenty, kt�re pami�tam
       urzek�y mnie na zawsze."
  start kom "mlci"
  justtalk
   D: "Mo�esz je wyrecytowa�?"
  juststay
  start kom "mluvi"
   U: "�ajdaku, bez wstydu z�ama�e� serce
       niewinnej dziewczyny."
  goto skip
   U: "Ona �piewa�a s�odk� pie�� o mi�o�ci,
       w kt�r� wierzy�a..."
   U: "...a ty j� wykorzysta�e�
       i nawet nie zap�aci�e�!"
   U: "Zas�ugiwa�a tylko na monety ze z�ota
       lecz ty by�e� przewrotny i z�y..."
   U: "...gdy si� u�miecha�a, powiniene� by� p�aka�
       dlatego te� w ko�cu zwr�ci�a
       swe serce ku mnie."
 label skip
  start kom "mlci"
  justtalk
   D: "Dalej!"
  juststay
  start kom "mluvi"
   U: "To koniec. W nast�pnym akcie znam
       tylko moj� kwesti�.
       To nie brzmi dobrze, recytowane| bez partnera..."
  start kom "mlci"

gplend

block _02 ( isactico(i_scenar) and not been(_02) and ( ((maxline(1) and beg(1))or last(_01)) )
title
 labels skip
  {kliknu na n�j sc�n��em:}
  load bert "scenar_mluvi"
  load bert "scenar_mlci"
  load bert "scenar_diva"
  load bert "scenar_vytah"
  load bert "scenar_schov"
  startplay bert "scenar_vytah"
  startplay bert "scenar_diva"
  start bert "scenar_mluvi"
   D: "Sta�o si� nieszcz��cie,
       pozosta� mi tylko szal,
       kt�rym mnie okrywa�a dziewica,
       gdy wzywa�a mnie ku sobie"
  start bert "scenar_mlci"
  start kom "mluvi"
   U: "Czy� nie omdlewa�a?"
  start kom "mlci"
  startplay bert "scenar_diva"
  goto skip
  start bert "scenar_mluvi"
   D: "Trudno oprze� si� pokusie
       by ujrze� j� pogr��on� w smutku!"
  start bert "scenar_mlci"
  start kom "mluvi"
   U: "Tylko jej usta m�wi�y bez s��w!"
  start kom "mlci"
  startplay bert "scenar_diva"
  start bert "scenar_mluvi"
   D: "Czy�by ona tylko udawa�a?"
  start bert "scenar_mlci"
  start kom "mluvi"
   U: "K�ama�a! Oszukiwa�a!"
  start kom "mlci"
 label skip
  start bert "scenar_mluvi"
   D: "Oklaski."
  start bert "scenar_mlci"
  start kom "mluvi"
   U: "O tak. By�e� doskona�y.
       Masz talent.
       Co s�dzisz o sztuce akrobat�w?"
  start kom "mlci"
  startplay bert "scenar_schov"
  start bert "kour-vlevo"
  justtalk
   D: "W cyrku albo w teatrze..."
  juststay
  start kom "mluvi"
   U: "A mo�e chcia�by� by� zaklinaczem w��y?"
  start kom "mlci"
  justtalk
   D: "O, w�a�nie!"
  juststay
  start kom "mluvi"
   U: "S�dz�, �e by�by� w tym dobry."
   U: "To jest klucz do mojej skrzyni.
       Wewn�trz znajdziesz gwizdek.
       Jest niezb�dny."
  start kom "mlci"

  icostat on i_klic_truhla
  icostat off i_scenar
gplend

block _04 ( maxline(1) and beg(1) and not been(_04) and isactico(i_loutka_drevena) )
title
  {kdy� vrac� loutky:)
  start kom "mluvi"
   U: "Hm, wtedy mia�em o tobie gorsze zdanie,
       a teraz widz�, �e z powrotem
       mo�esz zmieni�
       marionetki w drewniane."
  start kom "mlci"
  justtalk
   D: "Zaczarowa�em je niechc�cy!"
  juststay
  start kom "mluvi"
   U: "Kr�l wzlecia� na skrzyd�ach.
       Jack skaka� z rado�ci."
  start kom "mlci"
  justtalk
   D: "Co robisz?"
  juststay
  start kom "mluvi"
   U: "To na rozgrzewk�."
  start kom "mlci"
  justtalk
   D: "Co� bardziej tradycyjnego..."
  juststay
  start kom "mluvi"
   U: "Wp�yn��em na suchego przestw�r oceanu!"
  start kom "mlci"
  justtalk
   D: "�wietne!"
  juststay
  start kom "mluvi"
   U: "Dobranoc, pch�y na noc."
  start kom "mlci"
  justtalk
   D: "Jak ugryz� we� buta..."
  juststay
  start kom "mluvi"
   U: "I ubij je na czarno..."
  start kom "mlci"
  justtalk
   D: "To nie tak by�o!"
  juststay
  start kom "mluvi"
   U: "I ubij na niebiesko..."
  start kom "mlci"
  justtalk
   D: "Te� �le..."
  juststay
  start kom "mluvi"
   U: "I t�ucz je, a� zsiniej�."
  start kom "mlci"
  justtalk
   D: "Tak!"
  juststay

  icostat off i_loutka_drevena
gplend

block _08 ( maxline(1) and beg(1) )
title
  justtalk
   D: "Panie Artysto..."
  juststay
  start kom "mluvi"
   U: "Hmm?"
  start kom "mlci"
  justtalk
   D: "Ech, nic takiego..."
  juststay
  exitdialogue
gplend
