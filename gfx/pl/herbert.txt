{domluva otce s drakem v draci chalupe:}

block _00 ( beg(1) )
title
  icostat on i_varecka_zlomena
  load oteckaze "mluvi"
  load oteckaze "zlomluvi"
  load oteckaze "lame"
  load drakkaze "mluvi"

  start oteckaze "mluvi"
  start drakkaze "mlci"
   Herb: "...wiesz, synu, wstydz� si� teraz sam za siebie."
   Herb: "Ja, stary g�upiec!
          Jak mog�em dopu�ci� do siebie
          my�li o starodawnych skarbach smokow."
   Herb: "Tylko ty zapobieg�e� sk��ceniu naszej rodziny
          i uratowa�e� j� przed rozpadem."
   Herb: "Widz� teraz, �e doros�e�."
   Herb: "Mo�esz odt�d wypija� dwa piwa dziennie
          i chodzi� z dziewczynami."
  start oteckaze "mlci"
  start drakkaze "mluvi"
   D: "Z tym mo�na poczeka�, musz� przeprowadzi�
       moje poszukiwania."
   D: "I co b�dzie z moj� kar�?"
  start drakkaze "mlci"
  start oteckaze "mluvi"
   Herb: "To dobre pytanie."
   Herb: "Co pocz�� z twoj� kar�..."
   Herb: "Nie ukaram ci�. Okaza�o si�,
          �e jeste� bardziej rozs�dny ni� ja sam."
   Herb: "Nigdy ju� ci� nie odtr�c�."
   Herb: "A je�li chodzi o moje twarde s�owa..."
  startplay oteckaze "lame"
  start oteckaze "zlomil"

  start drakkaze "mluvi"
   D: "Nie cofaj ich. Chc� zachowa� pami��
       o moim psotnym dzieci�stwie..."
  start drakkaze "mlci"
  start oteckaze "zlomluvi"
   Herb: "To twoja sprawa."
  start oteckaze "zlomil"
  start drakkaze "mlci"
{zmiz� z dra�� chalupy}
gplend
