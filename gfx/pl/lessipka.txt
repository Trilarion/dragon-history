{s h�ze�em �ipek:(je dost zasmu�il�)}

block _00 ( beg(1) and not been(_00) and isactico(0) )
title
 labels skip
  justtalk
   D: "Ehm, czy�by� zamierza� si� powiesi�?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Tak."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Powiedz mi gdzie i kiedy, to przyjd�.
       Nigdy nie widzia�em prawdziwego wisielca."
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Zamiast nagrobka chc� mie� tarcz� do
        przyssawek..."
  start leskriz_sipkar "mlci"
  justtalk
   goto skip
   D: "Tarcz� do przyssawek?
       To brzmi troch� niekonwencjonalnie..."
  label skip
   D: "Mnie te� znudzi�y si� te szare nagrobki."
   D: "Je�li mam zgin�� w czasie
       tej niebezpiecznej misji,
       te� chc� tarcz� do przyssawek zamiast nagrobka!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Przyssawki nadaj� sens mej egzystencji.
        Bez nich nie ma �ycia!"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Czy to nie przesada?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Nie, z uwagi na znaczenie jakie maj� dla
        mnie przyssawki."
   S1: "Turniej Ligi Gry w Przyssawki powinien
        si� ju� dawno rozpocz��,
        ale ja nie mam przyssawek ani mo�liwo�ci treningu!"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Dlaczego?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Drzewo Przyssawkowe przesta�o wydawa� owoce."
{#it has something to do with darts, it is common, it is tree - Darttree Common
  start leskriz_sipkar "mlci"
  justtalk
   D: "Drzewo Przyssawkowe?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Tak, drzewo kt�re rodzi przyssawki,
        je�li mam ci� o�wieci� w tym wzgl�dzie,
        jest to Oficjalne drzewo Ligi Przyssawek"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Jak mog�o do tego doj��?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Nikt nie wie."
   S1: "Przyssawki na drzewie przyssawkowym
        dojrzewaj�
        tu� przed rozpocz�ciem rozgrywek Ligi Przyssawek."
   S1: "Pojawiaj� si� i dojrzewaj�
        w jeden dzie�."
   S1: "W tym roku jeszcze si� nie pojawi�y!"
  start leskriz_sipkar "mlci"
gplend

block _01 ( maxline(1) and beg(1) and isactico(0) )
title
  justtalk
   D: "Niech si� pan nie martwi..."
{#in czech it sounds like:don't hang your head
{#it has a lot of things to do with hanging
   D: "...Mam jedno pytanie."
  juststay
gplend

block _1 ( beg(0) and not been(_1) )
   title Nie wiedzia�em, �e przyssawki rosn� na drzewie!
  justtalk
   D: "Nie wiedzia�em, �e przyssawki rosn� na drzewie!"
   D: "My�la�em, �e sprzedaje si� je w sklepach!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "�mieszny pomys�.
        Ka�dy zdaje sobie spraw�, �e tak nie jest."
   S1: "Powiedz co� �miesznego,
        co ci przychodzi na my�l?"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Dzieci przynoszone s� rodzicom przez bociana!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Z pewno�ci�, a jak by�oby to mo�liwe inaczej?"
  start leskriz_sipkar "mlci"
gplend

block _2 ( beg(0) and not been(_2) )
   title Czy nie zostawi�e� sobie paru przyssawek...
  justtalk
   D: "Czy nie zostawi�e� sobie paru
       przyssawek na czarn� godzin�?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "To niemo�liwe."
   S1: "Zawsze psuj� si� przez zim�,
        dlatego lepiej przeznaczy� je jesieni�
        na kompost."
   S1: "Stanowi� doskona�y naw�z!"
  start leskriz_sipkar "mlci"
gplend

block _9 ( beg(0) and not been(_9) )
   title Jeste� prawdziwym entuzjast� przyssawek!
 labels skip
  justtalk
   D: "Jeste� prawdziwym entuzjast� przyssawek!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Ahem, Masz racj�.
        Skromnie m�wi�c, chcia�bym podkre�li�
        �e jestem mistrzem w Rzucaniu Przyssawek..."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Nie udawaj skromno�ci.
       Wszak to imponuj�ce!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "W domu mam ma�� wystaw�
        wszystkich moich trofe�w..."
   S1: "Lecz jak wiesz, kto� m�g�by je ukra��.
        Dlatego moje najcenniejsze trofeum
        zawsze nosz� przy sobie."
  start leskriz_sipkar "mlci"
  {vyt�hne z kapsy zlat� zvone�ek:}
  mark
  load leskriz_sipkar "zvonek"
  startplay leskriz_sipkar "zvonek"
  release

  start leskriz_sipkar "mluvi"
   S1: "Lepiej zaraz je schowam."
   S1: "Rzek�by� - zwyczajny dzwonek.
        Dosta�em go za drugie miejsce w jedynych
        zawodach,
        kt�rych nie wygra�em."
  goto skip
   S1: "Nie wiem,
        dlaczego mia�bym by� z�y z tego powodu."
   S1: "Wr�cz przeciwnie,
        jestem naprawd� dumny z tego dzwonka..."
 label skip
  start leskriz_sipkar "mlci"
gplend

block _7 ( beg(0) and not been(_7) )
   title A gdzie ro�nie Drzewo Przyssawkowe?
  justtalk
   D: "A gdzie ro�nie Drzewo Przyssawkowe?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Na wielkiej g�rze. Tak wielkiej, �e nie
        mo�na jej nie zauwa�y�."
  start leskriz_sipkar "mlci"
  {nov� lokace: ��pek}
  justtalk
   D: "Uwa�am,
       �e to ju� w�a�ciwa odpowiedz..."
  juststay
  let new_sipek (1)
gplend

block _8 ( beg(0) and not been(_8) )
   title Dlaczego sami nie pr�bowa�i�cie zrobi�...
 labels skip
  justtalk
   D: "Dlaczego sami nie pr�bowa�i�cie zrobi�
       czego� z Przyssawkowym Drzewem?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Oczywi�cie, pr�bowali�my!"
   S1: "Bezskutecznie"
  goto skip
   S1: "To tylko dowiod�o
        naszego braku kompetencji i g�upoty."
 label skip
   S1: "To jest zadanie dla bohatera."
  start leskriz_sipkar "mlci"
  justtalk
   D: "W kt�rym momencie wasze wysi�ki
       sko�czy�y si� fiaskiem?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Pr�bowali�my odszuka� w starych kronikach
        czy ju� kiedy�, w przesz�o�ci
        zaistnia� podobny problem."
   S1: "Jedyna kronika, kt�ra mog�aby co� wiedzie�
        od pi��dziesi�ciu lat nie chce z nami rozmawia�."
   S1: "Obrazi�a si�, bo �artowali�my sobie
        z niej, �e sepleni."
   S1: "I nikt nie mo�e sobie z ni� poradzi�."
   S1: "Przemawia�em do niej przez jaki� tydzie�."
   S1: "Najpierw by�em czu�y."
   S1: "Potem bezwzgl�dny!
        Pod koniec tygodnia ochryp�em."
   S1: "A ona tylko patrzy�a na mnie
        ze stoickim spokojem!"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Mo�e spr�bujemy poszuka� jakiego�
       specjalisty od wad wymowy.
       Gdzie jest ta kronika?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "U mnie w domu.
        Masz tu klucz."
  start leskriz_sipkar "mlci"
  justtalk
   D: "A gdzie ty mieszkasz?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Zaraz na prawo od twojego smoczego domku."
   S1: "Ale nie musisz mnie zna�,
        ca�y czas jestem na zawodach
        w rzucaniu przyssawek..."
  start leskriz_sipkar "mlci"

  icostat on i_klic_sipdum
  exitdialogue
gplend

block _3 ( beg(0) and not been(_3) and been(_2) )
   title Na nast�pny sezon powiniene�...
  justtalk
   D: "Na nast�pny sezon powiniene�
       zakonserwowa� troch� przyssawek!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Pr�bowali�my robi� z nich marmolad�."
   S1: "Lecz by�y tak pyszne,
        �e ka�dy musia� ich skosztowa�
        przed nadej�iem wiosny..."
   S1: "...i zanim rozpocz�� si� nowy sezon,
        marmolada by�a ju� zjedzona."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Co, tak w og�le, robicie z przyssawkami?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Po�ow� zu�ywamy by nawozi�
        drzewo przyssawkowe
        �eby w nast�pnym roku wydawa�o
        dobre przyssawki,
        a drug� po�ow� zawsze konserwujemy."
   S1: "Jest taka tradycja, by w wiecz�r
        przed rozpocz�ciem rozgrywek
        Ligi Przyssawek
        spo�ywa� marmolad� na przyj�ciu."
  start leskriz_sipkar "mlci"
  justtalk
   D: "To dlatego przyssawki zosta�y zjedzone
       ju� w zeszlym roku!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Tak jest."
   S1: "Wszyscy naprawd� rozkoszowali�my si� ostatnimi,
        zesz�orocznymi przyssawkami.
        By�y naprawd� wy�mienite."
   S1: "Wci�� b�d�c w narkotycznym transie pobiegli�my
        pod przyssawkowe drzewo."
  start leskriz_sipkar "mlci"
  justtalk
   D: "W transie?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Nie wiesz, �e konserwowane przyssawki daj�
        Hmmm, ciekawe efekty?
        Hmmm,..."
   S1: "Czekali�my tam przez jaki� tydzie�,
        lecz bez skutku."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Patrzyli�cie czy przyssawki nie wyrastaj�, i nic?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Prawd� m�wi�c,
        ca�y czas spali�my."
   S1: "Po tygodniu, kiedy pierwsi gracze
        zbudzili si� z upojenia,
        spostrzegli, �e na drzewie nie ma wcale
        przyssawek..."
   S1: "... i �e na ziemi te� nie ma,
        tylko dru�yna przeciwnik�w
        �pi pod przyssawkowym drzewem."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Mo�e �le dobrali�cie proporcje, ile
       przyssawek da� na kompost, a ile zakonserwowa�..."
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Raczej w�tpi�!
        Wszystko robili�my tak samo jak w zesz�ym roku."
  start leskriz_sipkar "mlci"
gplend

block _6 ( beg(0) and not been(_6) and been(_3) )
   title Czym mieszali�cie konserwowane przyssawki?
  justtalk
   D: "Czym mieszali�cie konserwowane przyssawki?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Nie mieli�my �y�ki,
        wi�c u�yli�my drewnianego patyczka."
   S1: "Ci�gle gada� i gada�,
        ale on si� sam zg�osi� na ochotnika!"
  start leskriz_sipkar "mlci"
  justtalk
   D: "A co, je�li to ten patyk by� powodem problem�w?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "W�tpi�."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Czym mieszali�cie kompost?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Widelcem."
   S1: "Zwyk�ym widelcem."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Czy widelec m�wi�?"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "NIE!"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Do diab�a, czuj� si� zbity z tropu!"
   D: "I ju� naprawd� nie wiem co pocz��..."
  juststay
gplend

block _03 ( beg(0) and not last(_01) and not last(_00) )
   title No to do widzenia.
  justtalk
   D: "No to do widzenia."
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Do widzenia."
  start leskriz_sipkar "mlci"
  justtalk
   D: "Mam nadziej�, �e NAPRAWD� zn�w si� spotkamy!"
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Wierz w to, ch�opcze..."
  start leskriz_sipkar "mlci"
  exitdialogue
gplend

block _02 ( maxline(1) and beg(0) and last(_01) )
title
  justtalk
   D: "Chcia�bym wiedzie� co by tu pocz��!"
  juststay
   exitdialogue
gplend

block _91 ( maxline(1) and beg(1) and isactico(i_sipky) )
title
  icostat off i_sipky
  justtalk
   D: "Mam dla ciebie dobre wie�ci,
       Mam przyssawki."
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Czy drzewo zn�w rodzi owoce?"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Tak."
  juststay
  start leskriz_sipkar "mluvi"
   S1: "Jak to si� sta�o, �e odzyska�o zdrowy rozs�dek?"
  start leskriz_sipkar "mlci"
  justtalk
   D: "Nie wiem, drzewo co� sobie pomy�li i nagle..."
  juststay
  start leskriz_sipkar "mluvi"
   S1: "To wspaniale, teraz musz� nabra� kondycji."
   S1: "Dzi�kuj�, i mam nadziej�, �e si� zobaczymy
        pewnego dnia na przyssawkowych zawodach!"
  start leskriz_sipkar "mlci"
  load leskriz_sipkar "odchazi"
  startplay leskriz_sipkar "odchazi"
  {odejde...}
  {m�l by odej�t!!!}

  let predal_sipky (1)
  objstat away leskriz_sipkar
  exitdialogue
gplend
