{Bert s hlavn� �idl�:}

{kliknuti cihlou chytneme hned na zacatku:}
block _11 ( not last(_11) and ((beg(1) and maxline(1) and isactico(i_cihla))or beg(0)) and (nedosahnu_na_pohadku=1) and isobjon(pohadka) and isobjon(hodzid_dobra) )
   title Prosz� by� mi troch� pomog�a...
 labels nemamcihlu
  justtalk
   D: "Prosz� by� mi troch� pomog�a..."
  juststay
  start hodzid "mluvi"
   Ka: "W czym?"
  start hodzid "mlci"
  justtalk
   D: "Po prostu stan� na tobie..."
  juststay
  start hodzid "mluvi"
   Ka: "Och!"
  start hodzid "mlci"
  justtalk
   D: "Musz� zwr�ci� ci uwag�,
       �e to bardzo ryzykowne."
   D: "Mo�e stan�
       tylko na jednej nodze!"
  juststay
  start hodzid "mluvi"
   Ka: "Ale ja mam trzy nogi!"
  start hodzid "mlci"

  if(isactico(0)) nemamcihlu
  justtalk
   D: "Nie martw si�, �e masz tylko trzy nogi.
       Znalaz�em co�, co mo�na pod ciebie pod�o�y�."
  juststay
  start hodzid "mluvi"
   Ka: "Co to? Stara ceg�a?"
  start hodzid "mlci"
  justtalk
   D: "Chyba si� nada."
  juststay

   icostat off i_cihla
{   icostat on i_pohadka
{   objstat away pohadka
{do obou identifikatoru dam, ze uz tam pohadka neni
{   let nedosahnu_na_pohadku (2)
{   let vim_o_pohadce (2)
{nyni zaridit situacku s vyndanim knihy:}
   newroom hod 2
   exitdialogue
 label nemamcihlu
gplend

block _00 ( maxline(1) and beg(1) and not been(_00) )
title
  justtalk
   D: "Czy to wy, Wasza Wysoko��?"
  juststay
  start hodzid "mluvi"
   Ka: "Tak, to naprawd� ja, Karmela Pi�ta,
        nawet w tej n�dzy pozostaj� sob�!
        Czy przyby�e� by mnie uwolni�?"
  start hodzid "mlci"
  justtalk
   D: "(Mam do�� w�asnych zmartwie�.
       Mo�e innym razem.
       Musz� szybko z tym sko�czy�!)"
   D: "Wygl�dasz
       na strasznie wychudzon�."
  juststay
  start hodzid "mluvi"
   Ka: "Nic dziwnego..."
  start hodzid "mlci"
gplend

block _01 ( maxline(1) and beg(1) and not been(_01) and been(_00) )
title
  justtalk
   D: "Karmelo Pi�ta!"
  juststay
  start hodzid "mluvi"
   Ka: "S�ucham.
        Czym mog� s�u�y�?"
      start hodzid "mlci"
gplend

block _02 ( maxline(1) and beg(1) and been(_01) )
title
  justtalk
   D: "Karmelo Pi�ta!"
  juststay
  start hodzid "mluvi"
   Ka: "Tak?"
  start hodzid "mlci"
gplend

block _10 ( beg(0) and not been(_10) )
   title Twoje damy prosi�y, abym tu przyby�.
  justtalk
   D: "Twoje damy prosi�y, abym tu przyby�."
  juststay
  start hodzid "mluvi"
   Ka: "Masz jakie� nowiny?"
  start hodzid "mlci"
  justtalk
   D: "Melania jest ca�kiem po�amana."
  juststay
  start hodzid "mluvi"
   Ka: "Nic dziwnego."
  start hodzid "mlci"
  justtalk
   D: "Eulania skar�y si�,
       �e pe�no w niej kornik�w."
  juststay
  start hodzid "mluvi"
   Ka: "Ona narzeka na to conajmniej od 50 lat!"
  start hodzid "mlci"
  justtalk
   D: "Agata pr�chnieje."
  juststay
  start hodzid "mluvi"
   Ka: "Czy prze�yje?"
  start hodzid "mlci"
  justtalk
   D: "Przykro mi, jest wyko�czona..."
  juststay
  start hodzid "mluvi"
   Ka: "Och, moja najlepsza przyjaci��ka!"
  start hodzid "mlci"
gplend

block _14 ( beg(0) and not been(_14) and isobjon(hodzid_dobra) and not maxline(1) )
   title Jak si� czujesz po tej drobnej naprawie?
  justtalk
   D: "Jak si� czujesz po tej drobnej naprawie?"
  juststay
  start hodzid "mluvi"
   Ka: "Jak nowo narodzona!
        Ta �ata jest wspania�a."
      start hodzid "mlci"
  start hodzid "mluvi"
   Ka: "Jestem ci wielce zobowi�zana."
  start hodzid "mlci"
gplend

block _13 ( not last(_13) and beg(0) and (nedosahnu_na_pohadku=1) and isobjon(pohadka) and not isobjon(hodzid_dobra) )
   title Chcia�bym, �eby� mi pomog�a...
  justtalk
   D: "Chcia�bym, �eby� mi pomog�a..."
  juststay
  start hodzid "mluvi"
   Ka: "Naprawd�?"
   Ka: "Nie s�dz� aby� zas�ugiwa�
        na jak�� pomoc z mojej strony.
        Przykro mi. Nie mog�."
      start hodzid "mlci"
gplend

block _1 ( beg(0) and not been(_1) )
   title Z pewno�ci� skrzypisz!
  justtalk
   D: "Z pewno�ci� skrzypisz!"
  juststay
  start hodzid "mluvi"
   Ka: "Nie tylko."
  start hodzid "mlci"
  justtalk
   D: "Jedna z twoich n�g jest kr�tsza
       od pozosta�ych..."
  juststay
  start hodzid "mluvi"
   Ka: "To zwyk�a kolej rzeczy..."
  start hodzid "mlci"
gplend

block _2 ( beg(0) and not been(_2) )
   title Jak si� tu znalaz�a�?
  justtalk
   D: "Jak si� tu znalaz�a�?"
  juststay
  start hodzid "mluvi"
   Ka: "Zosta�am aresztowana przez �o�nierzy
        pana Pi�talusa."
  start hodzid "mlci"
  justtalk
   D: "Nie powinna� by�a pozwoli�, by ci� zabrali."
  juststay
  start hodzid "mluvi"
   Ka: "Pr�bowa�am wbi� jednemu drzazg�,
        lecz mia� na sobie
        zbroj�."
      start hodzid "mlci"
gplend

block _3 ( beg(0) and not been(_3) )
   title Ciekaw jestem, czy torturuj� ci� w jaki�...
  justtalk
   D: "Ciekaw jestem,
       czy zadaj� ci jakie� specjalne tortury?"
  juststay
  start hodzid "mluvi"
   Ka: "Pi�talus buja mnie codziennie po obiedzie
        przez p�� godziny!"
      start hodzid "mlci"
  justtalk
   D: "To wstr�tne!"
  juststay
gplend

block _4 ( beg(0) and not been(_4) )
   title Czemu ci� aresztowali?
  justtalk
   D: "Czemu ci� aresztowali?"
  juststay
  start hodzid "mluvi"
   Ka: "My�l�, �e to wszystko przez Otoman�..."
{#Otoman - this is a name, but it is also a special kind of sofa
  start hodzid "mlci"
  justtalk
   D: "Przez kogo?"
  juststay
  start hodzid "mluvi"
   Ka: "Czy kiedykolwiek s�ysza�e� imi�
        Emrusa Otomana?!"
   Ka: "Nie znasz legendarnego
        wodza wszystkich krzese�?"
   Ka: "Ka�dy sto�ek go zna!"
  start hodzid "mlci"
gplend

block _5 ( beg(0) and not been(_5) and been(_4) )
   title Jak otomana mo�e by� wodzem krzese�?
  justtalk
   D: "Jak otomana mo�e by� wodzem
       wszystkich krzese�?"
  juststay
  justtalk
   D: "Przecie� nie jest krzes�em!"
  juststay
  start hodzid "mluvi"
   Ka: "Otomana by� krzes�em!"
   Ka: "Pochodzi� ze starodawnego
        rodu krzese�, jego prababcia
        by�a le�ank�."
  start hodzid "mlci"
gplend

block _6 ( beg(0) and not been(_6) and been(_4) )
   title Opowiedz mi co� o dziejach krzese�!
  justtalk
   D: "Opowiedz mi co� o dziejach krzese�!"
  juststay
  start hodzid "mluvi"
   Ka: "No... dobrze..."
   Ka: "Dawno temu
        krzes�a �y�y w niewoli u stwor�w
        kt�re -"
   Ka: "- wiem, �e w obecnych czasach mo�na
        uwa�a� to za �mieszne -
        kt�re mia�y tylko dwie nogi."
  start hodzid "mlci"
  justtalk
   D: "To zabawne!"
  juststay
  start hodzid "mluvi"
   Ka: "Tak.
        Przyby� jednak dzielny Emrus."
   Ka: "Wszcz�� bunt
        przeciw ciemi��ycielom, i to w�a�nie on
        sformu�owa� nasz�, znan� dzi� dobrze maksym�."
  start hodzid "mlci"
gplend

block _7 ( beg(0) and not been(_7) and been(_6) )
   title Jak zako�czy�o si� powstanie?
  justtalk
   D: "Jak zako�czy�o si� powstanie?"
  juststay
  start hodzid "mluvi"
   Ka: "Nadesz�a bardzo sroga zima i si�y powsta�c�w
        ponios�y powa�ne straty."
  start hodzid "mlci"
  justtalk
   D: "Czy krzes�a zamarz�y w czasie
       dzia�a� wojennych?"
  juststay
  start hodzid "mluvi"
   Ka: "Nie, zosta�y spalone!"
  start hodzid "mlci"
gplend

block _8 ( beg(0) and not been(_8) and been(_6) )
   title Jak brzmia�a maksyma Emrusa?
  justtalk
   D: "Jak brzmia�a maksyma Emrusa?"
  juststay
  start hodzid "mluvi"
   Ka: "Cztery nogi to nie mniej ni� dwie!"
  start hodzid "mlci"
  justtalk
   D: "Brzmi to imponuj�co!
       (Chyba ju� przedtem gdzie� to s�ysza�em...)"
  juststay
  start hodzid "mluvi"
   Ka: "Jeste� wszak oczytanym intelektualist�, prawda?"
  start hodzid "mlci"
gplend

block _9 ( beg(0) and not been(_9) and been(_7) )
   title Jak zgin�� Emrus Otoman?
  justtalk
   D: "Jak zgin�� Emrus Otoman?"
  juststay
  start hodzid "mluvi"
   Ka: "Najpierw poci�li go na drobne szczapki
        i wyko�czyli
        latem, podczas pieczenia kie�basek."
  start hodzid "mlci"
  justtalk
   D: "Co za ha�ba!"
   D: "Nie wytrzyma�bym
       tego t�uszczu topi�cego si� na moim ciele!"
  juststay
  start hodzid "mluvi"
   Ka: "Lecz jego spu�cizna pozosta�a w�r�d nas."
   Ka: "Odt�d krzes�a
        nie cierpia�y ju� d�u�ej."
      start hodzid "mlci"
gplend

block _99 ( beg(0) and not(last(_01) or last(_02) or last(_11) ) and not maxline(1) )
   title Musz� i��, �ycz� ci mi�ego dnia...
  justtalk
   D: "Musz� i��, �ycz� ci mi�ego dnia..."
  juststay
  start hodzid "mluvi"
   Ka: "Ja tobie te�."
  start hodzid "mlci"
  exitdialogue
gplend

block _98 ( beg(0) and maxline(1) and (last(_01) or last(_02)) )
title
  justtalk
   D: "No, nie �artuj."
  juststay
  start hodzid "mluvi"
   Ka: "To kiepski dowcip, prawda?
        Nie kpij sobie ze mnie, prosz�!"
      start hodzid "mlci"
  exitdialogue
gplend
