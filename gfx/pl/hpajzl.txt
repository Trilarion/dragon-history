{s pajzlem v hospod�:}
block _00 ( beg(1) and not been(_00) )
title
  justtalk
   D: "Panie Krasnalu..."
   D: "Czy zechce pan kupi� mi piwo?"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Wyno� si�."
  start hpajzl_hlava "mlci"
  justtalk
   D: "Nie tak �atwo mnie nastraszy�!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "To piwo jest ciep�e!"
  start hpajzl_hlava "mlci"
  justtalk
   D: "Na Boga, ile tego masz!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Wyno� si� ch�opcze!"
  start hpajzl_hlava "pije"
  justtalk
   D: "Nie jeste� zbyt rozmowny...
       Wygl�dasz na zm�czonego."
   D: "Z pewno�ci� mia�e� jakie� straszne prze�ycia!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "To nie twoja rzecz!"
  start hpajzl_hlava "mlci"
  justtalk
   D: "Musisz to z siebie wyrzuci�..."
  juststay
  start hpajzl_hlava "mluvi"
   P: "Dobra,
       gdzie tu jest ubikacja?"
  start hpajzl_hlava "mlci"
  justtalk
   D: "...te wszystkie straszne do�wiadczenia,
       kt�re si� w tobie gromadzi�y!"
  juststay
gplend

block _00b ( last(_00) )
title
 labels skip
  start hpajzl_hlava "mluvi"
   P: "Piwo!"
  start hpajzl_hlava "pije"
  justtalk
   D: "Nie pr�buj zatopi� swoich problem�w w alkoholu!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Co?"
  goto skip
  start hpajzl_hlava "mlci"
  justtalk
  D: "Znacznie lepiej jest topi� problemy
      w narkotykach!!"
  juststay
  start hpajzl_hlava "mluvi"
  P: "Naprawd�?
      Musz� spr�bowa�."
 label skip
   P: "Teraz zacz��em rozumie�.
       Otworzy�e� mi oczy."
  start hpajzl_hlava "mlci"
  justtalk
   D: "A czy teraz mi to wyznasz?"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Tak.
       Mo�e lepiej
       gdy ci to wyznam."
   P: "Unikn��em pewnej �mierci!"
   P: "O ma�y w�os nie zosta�em po�arty."
   P: "I tyle."
  start hpajzl_hlava "mlci"
  justtalk
   D: "Co ty powiesz!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "To okropne, jak kto� mo�e
       w dzisiejszych czasach
       dopuszcza� si� po�erania krasnali."
   P: "Tak, s� kraje gdzie
       po�eranie krasnoludk�w jest zakazane
       ju� od wielu lat."
  start hpajzl_hlava "mlci"
gplend

block _00c ( last(_00b) )
title
 labels skip
  start hpajzl_hlava "mluvi"
   P: "A czy� my nie jeste�my
       krajem cywilizowanym, synu?"
  start hpajzl_hlava "mlci"
  justtalk
   D: "Ach, to skandal na skal� mi�dzynarodow�."
  juststay
  start hpajzl_hlava "mluvi"
   P: "Chyba zgodzisz si� ze mn�."
   P: "Nie chodzi tylko o mnie,
       lecz te� o moich biednych braci,
       c�� ich czeka?"
  goto skip
   P: "A co b�dzie ze mn�?"
  start hpajzl_hlava "mlci"
  justtalk
   D: "Czy spodziewaj� si� jaki� strasznych rzeczy?"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Tak, co b�dzie ze mna?
       I z nimi?"
  label skip
   P: "Najpierw posypie ich pieprzem."
   P: "Potem zbije, ut�ucze."
   P: "Potem udusi."
   P: "Mo�e usma�y."
   P: "Ugh!"
   P: "Ja nie m�g�bym tego zrobi�,
       cholesterol mi szkodzi!"
   P: "Albo nasypie im do oczu tartej bu�ki."
   P: "Nast�pnie poleje ich sokiem z cytryny..."
  start hpajzl_hlava "pije"
  justtalk
   D: "Burczy mi w brzuchu.
       Gdzie m�g�bym spr�bowa� tej egzotycznej potrawy?"
   D: "Czy zaprosisz mnie na przyj�cie?"
  juststay
  {pajzl odpov�d� velice smutn� a zklaman�:}
  start hpajzl_hlava "mluvi"
   P: "Nie zaprosz�. Jestem w rozpaczy."
  start hpajzl_hlava "pije"
gplend

block _00d ( last(_00c) )
title
  justtalk
   D: "Przepraszam. Chyba ci� nie urazi�em?
       Gdzie s� twoi bracia?"
   D: "Uwolni� ich!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Naprawd� chcesz to uczyni�?"
  start hpajzl_hlava "mlci"
  justtalk
   D: "Naturalnie."
  juststay
  start hpajzl_hlava "mluvi"
   P: "Moi bracia
       oczekuj� na po�arcie..."
   P: "...w strasznym legowisku jeszcze bardziej
       straszliwego olbrzyma."
  start hpajzl_hlava "mlci"
  justtalk
   D: "Gdzie, dok�adnie?"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Na p��nocny wsch�d st�d."
   P: "Je�li chcesz wiedzie� dok�adnie
       pi��set stopni i siedemdziesi�t pi��
       godzin... przepraszam, minut."
  start hpajzl_hlava "mlci"
  justtalk
   D: "Trudno mi b�dzie znale�� to miejsce!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "I sze��dziesi�t sekund."
  start hpajzl_hlava "mlci"
  justtalk
   D: "Starczy!
       Wiem gdzie to jest."
   D: "Id�!"
  juststay
  start hpajzl_hlava "mluvi"
   P: "Powodzenia.
       Powiedz moim braciom,
       �e czekam tu na nich."
   P: "Je�li nie b�d� mogli mnie znale��,
       to jestem pod sto�em."
  start hpajzl_hlava "mlci"
{hodime semka newroom do mapy- objeveni se lokace s obrem}
{a z mapy opet gejt semka...}
  let new_obr (1)
gplend

block _01 ( beg(1) and been(_00) )
title
 labels skip
  justtalk
   D: "Panie Krasnalu..."
  juststay
  start hpajzl_hlava "mluvi"
   P: "Co z moimi bra�mi?"
  start hpajzl_hlava "mlci"
  justtalk
   D: "W porz�dku."
  goto skip
   D: "Dobrze si� bawi�."
 label skip
  juststay
  start hpajzl_hlava "mluvi"
   P: "Czemu ich tu nie przyprowadzi�e�?"
  start hpajzl_hlava "mlci"
  justtalk
   D: "Gdybym zepsu� im wakacje
       do ko�ca �ycia czu�bym si� g�upio."
   D: "Nie mo�esz mnie o to prosi�."
  juststay
  start hpajzl_hlava "mluvi"
   P: "Wyno� si�."
  start hpajzl_hlava "mlci"
gplend
