{Potuln� kouzeln�k Apokalypso:}

block _09 ( maxline(1) and beg(1) and isactico(i_karty) and not been(_09) )
title
  disablequickhero
  load bert "karty_mluvi"
  load bert "karty_mlci"
  load bert "karty_vytah"
  load bert "karty_schova"
  load bert "karty_vejnah"
  load bert "karty_vejir"
  load bert "karty_vejdol"
  load bert "karty_micha"
  load bert "karty_ukanah"
  load bert "karty_ukazuj"
  load bert "karty_ukadol"

  justtalk
    D: "Ju� zgromadzi�em do�� mocy magicznej!"
  juststay
  start apok "mluvi"
    A: "Ja te�, mo�emy zacz�� nasze zawody
        Co przygotowa�e�?"
  start apok "mlci"
  justtalk
    D: "To tylko ma�a sztuczka -
        tylko cz�stka
        mych magicznych umiej�tno�ci."
    D: "Lecz jest ona w naszej rodzinie przekazywana
        z pokolenia na pokolenie..."
  juststay
  walkonplay 116 164 vpravo
{vyt�hne bal��ek karet a rozev�e ho do v�j��e:}
  startplay bert "karty_vytah"
  start bert "karty_mluvi"
    D: "Wybierz kart�..."
  startplay bert "karty_vejnah"
  start bert "karty_vejir"

{kouzeln�k uk��e na kartu:}
  mark
  load apok "ukaznahoru"
  startplay apok "ukaznahoru"
  load apok "ukazmluvi"
  start apok "ukazmluvi"
    A: "Chc� t�."
  load apok "ukazdolu"
  startplay apok "ukazdolu"
  release
  start apok "mlci"

  startplay bert "karty_vejdol"
  start bert "karty_mluvi"
    D: "A teraz potasujmy karty."
  startplay bert "karty_micha"
  start bert "karty_mluvi"
    D: "Czy to by�a ta?"
  startplay bert "karty_ukanah"
  start bert "karty_ukazuj"

  start apok "mluvi"
    A: "Wcale nie!
        To by�a dziesi�tka kier."
  start apok "mlci"

  startplay bert "karty_ukadol"
  start bert "karty_mluvi"
    D: "Oszukujesz, w tej talii s� tylko asy.
        To musia� by� as!
        Czy dobrze si� przyjrza�e�?"
  start bert "karty_mlci"

  start apok "mluvi"
    A: "Chyba nie."
  start apok "mlci"
  start bert "karty_mluvi"
    D: "W�a�nie, to musia� by� as."
  start bert "karty_mlci"
  start apok "mluvi"
    A: "Masz racj�!
        Tak, masz racj�, to by� as,
        wszystko popsu�em."
    A: "Maj�c twoje umiej�tno�ci
        nie by�bym taki skromny!"
  start apok "mlci"
  startplay bert "karty_schova"
  start bert "kour-pravo"
  walkonplay 100 164 vlevo
  justtalk
    D: "Nie zas�uguj� na taki komplement..."
  juststay
  {Drak by mohl z�ervenat v ksicht�- a pak zase zezelenat
  justtalk
    D: "Teraz kolej na ciebie.
        To by� tamten kamie�, nieprawda�?"
  juststay
  start apok "mluvi"
    A: "Nie wiem, czy� przedtem nie wygl�da� on
        nieco inaczej?"
  start apok "mlci"
  justtalk
    D: "To z powodu erozji.
        Wiej� tu do�� silne wiatry."
  juststay
  {a zvedne se poryv v�tru, kter� m��e odn�st
  {kouzeln�kovi �epici, nebo nadzvednout pl���-vida! mohl by
  {m�t k�iv� nohy a �erven� tren�rky! sta�ily by ty
  {tren�rky...
  exitdialogue
gplend

block _09a ( maxline(1) and beg(1) and isactico(i_karty) and not been(_09a) )
title
  disablequickhero

  start apok "mluvi"
    A: "A wi�c - niech ten kamie�
        zmieni si� w py�..."
{kouzlime:}
  load apok "kouzli"
  load apok "mlci1sec"
  startplay apok "kouzli"

  startplay apok "mlci1sec"
  {nic se ned�je, chv�li pauza, jenom oba civ� na k�men
  start apok "mluvi"
    A: "Chc� by ten kamie� zmieni� si� w py�!"

  startplay apok "kouzli"
  startplay apok "kouzli"
  start apok "mlci"
{zase se nic ned�je, drak se pod�v� t�zav� na kouzeln�ka
  start apok "mluvi"
    A: "Rozkazuj� by ten kamie� zmieni� si� w py�!"
  startplay apok "kouzli"
  startplay apok "mlci1sec"
  startplay apok "mlci1sec"

  start apok "mlci"
  {nic se ned�je, chv�li pauza, jenom oba civ� na k�men
  start apok "mluvi"
    A: "Je�li ten kamie�
        natychmiast nie zmieni si� w py�
        zamieni� w py� ca�� ziemi�!"
  startplay apok "kouzli"
  start apok "mlci"
{dokouzlili jsme}
  exitdialogue
gplend

block _09b ( maxline(1) and beg(1) and isactico(i_karty) and not been(_09b) )
title
  {shora ude�� blesk do kamene a ten se prom�n�
    load myt_blesk "FLI-animace"
    load loutka_kam "promena"
    objstat_on loutka_dre myt
    start loutka_dre "zakladni"
    start loutka_kam "promena"
    startplay myt_blesk "FLI-animace"
    objstat away loutka_kam
  exitdialogue
gplend

block _09c ( maxline(1) and beg(1) and isactico(i_karty) and not been(_09c) )
title
  disablequickhero
  load apok "mlci1sec"
  walkonplay 100 168 vpravo
  justtalk
    D: "Zdziwi�bym si�, gdyby to zadzia�a�o."
  juststay
  start apok "mluvi"
    A: "Ta gro�ba zawsze skutkuje."
  startplay apok "mlci1sec"
  startplay apok "mlci1sec"
  start apok "mlci"
  {Chvilka ml�en�}
  justtalk
    D: "Kto wygra�?
        G�osuj� na siebie."
  juststay
  start apok "mluvi"
    A: "Jestem zaszczycony, lecz g�osuj� na siebie."
  start apok "mlci"
  justtalk
    D: "A wi�c remis?"
  juststay
  start apok "mluvi"
    A: "Remis."
    A: "Lecz podoba mi si� to twoje zakl�cie.
        M�g�by� mnie go nauczy�?"
  start apok "mlci"
  justtalk
    D: "Mo�e innym razem..."
  juststay

  icostat off i_karty
  exitdialogue
gplend

block _07 ( beg(1) and isactico(i_rad) and not been(_07) )
title
  disablequickhero
  load bert "rad_vytahuje"
  load bert "rad_schovava"
  load bert "rad_mluvi"
  load bert "rad_mlci"
  justtalk
    D: "Tu jest moja opinia
        ze szko�y czarodziei.
        Otrzymywa�em nagrody i wyr��nienia."
    D: "Dosta�em nawet list pochwalny!"
  juststay
  start apok "mluvi"
    A: "Przpomina z wygl�du wezwanie do wojska,
        znam te marne sztuczki."
    A: "Wpierw mi go poka�."
  start apok "mlci"
  {uk��e mu v�puj�n� ��d z knihovny}
  walkonplay 116 164 vpravo
  startplay bert "rad_vytahuje"
  start bert "rad_mluvi"
    D: "To nie wezwanie do wojska, lecz..."
    D: "Widzisz?"
    D: "Same pi�tki!"
  start bert "rad_mlci"
{kouzelnik zabrejli na rad:}
  load apok "kouka"
  startplay apok "kouka"

  start apok "mluvi"
    A: "Dobrze, a co to jest,
        dosta�e� 4 z odczytywania magicznych zakl��."
    A: "Kiepsko!"
  start apok "mlci"
  start bert "rad_mluvi"
    D: "To by� ma�o wa�ny przedmiot.
        Popatrz na �redni�."
  start bert "rad_mlci"
{kouzelnik zabrejli na rad:}
  startplay apok "kouka"

  start apok "mluvi"
    A: "4.30, hm, to nie tak �le.
        Widz�, �e mo�emy wsp��zawodniczy�."
  start apok "mlci"
  startplay bert "rad_schovava"
  start bert "kour-pravo"
  walkonplay 112 164 vpravo
  start apok "mluvi"
   A: "Zacznijmy od zaraz,
       jakie zakl�cie przygotowa�e�?"
  start apok "mlci"
  justtalk
   D: "Ehm..."
   D: "...przygotowa�em..."
   D: "...musz� jeszcze sk�d� zaczerpn�� mocy."
   D: "Chc� si� rozejrze� i poszuka�."
  juststay
  let prokazal_kvalifikaci (2)
  icostat off i_rad
  exitdialogue
gplend

block _00a ( beg(1) and been(_00b) and isactico(0) )
title
  justtalk
   D: "Apokalypso..."
  juststay
gplend

block _00b ( beg(1) and not been(_00b) and isactico(0) )
title
  justtalk
   D: "Ch�opie!
       Czy trzeba ci w czym� pom�c?"
  juststay
  start apok "mluvi"
   A: "Pom�c?
       Czy widzia�e� kiedy�
       jak kto� pobiera magiczn� moc?"
   A: "Ja ci w tym nie pomog�.
       Sam mam jej tyle, �e mi wystarczy"
    A: "Je�li chcesz zaczerpn�� troch� mocy, to
        prosz� bardzo -
        - lecz znajd� sobie jakie� inne miejsce."
  start apok "mlci"
  justtalk
   D: "Szkoda, my�la�em �e si� ni� podzielisz."
  juststay
  start apok "mluvi"
   A: "Naprawd�?"
  start apok "mlci"
  justtalk
   D: "Szkoda, ale
       przynajmniej poznali�my si�."
   D: "Nazywaj� mnie Bert."
   D: "Na imi� mi Bert!"
  juststay
  start apok "mluvi"
   A: "Na imi� mi Apokalypso."
   A: "Na imi� mi Apokalypso.|"
  start apok "mlci"
gplend

block _0011 ( beg(0) and not been(_0011) )
    TITLE Czy widzia�e� tu gdzie� czarodziejsk� r��d�k�?
  justtalk
    D: "Czy widzia�e� tu gdzie� czarodziejsk� r��d�k�?"
  juststay
  start apok "mluvi"
    A: "Zwariowa�e�?
        Nie potrzebuj� czarodziejskich r��d�ek."
   A: "Mog� si� bez nich oby�."
  start apok "mlci"
  justtalk
   D: "Czy czynisz czary go�ymi r�kami?"
  juststay
  start apok "mluvi"
   A: "Czasem nawet gromadz�
       tak pot��ne si�y magii,
       �e pal� sobie palce!"
   A: "To dlatego tak ci dr�� r�ce."
  start apok "mlci"
gplend

block _02 ( beg(0) and not been(_02) and not been(_09) )
    TITLE A wi�c jeste� czarodziejem, nieprawda�?
  justtalk
   D: "A wi�c jeste� czarodziejem, nieprawda�?"
  juststay
  start apok "mluvi"
   A: "Sk�d, na Boga, si� o tym dowiedzia�e�?"
  start apok "mlci"
  justtalk
   D: "Pozna�em po twoim kapeluszu!"
  juststay
  start apok "mluvi"
   A: "No dobrze, powiadaj� �e jestem czarodziejem."
  start apok "mlci"
  justtalk
   D: "Kto tak powiada?"
  juststay
  start apok "mluvi"
   A: "Hm, o ile wiem,
       ja sam, a ponadto..."
   A: "... wielu innych."
  start apok "mlci"
  justtalk
   D: "Zaje�o by to z pewno�ci� p�� dnia
       gdyby chcie� wymieni� ich wszystkich po imieniu."
   D: "Widz�, �e masz jak najlepsze referencje."
  juststay
  start apok "mluvi"
   A: "Powiedzmy.
       I po co mam traci� czas na gadanie."
   A: "To z pewno�ci� wyczerpuje
       cz��� mej magicznej energii
       i sprawia to mi du�� przykro��."
   A: "Wszystko, tylko nie to."
  start apok "mlci"
gplend

block _03 ( (1=2) and beg(0) and not been(_03) and not been(_07) )
title
   D: "."
   D: "."
   A: "."
   A: "."
   A: "."
   D: "."
   D: "."
   D: "."
   A: "."
gplend

block _04 ( beg(0) and not been(_04) )
   TITLE Czy nie masz ju� do�� tych czar�w?
  justtalk
   D: "Czy nie masz ju� do�� tych czar�w?"
  juststay
  start apok "mluvi"
   A: "Wcale nie!
       Pr�buj� rzuci� ca�kiem nowe zakl�cie."
   A: "Jeszcze bardziej fantastyczne i cudowne
       ni� wszystko, co dotychczas uczyni�em."
  start apok "mlci"
  justtalk
   D: "A co to ma by�?"
  juststay
  start apok "mluvi"
   A: "Nie wiem, czy mog� ci to zdradzi�?"
  start apok "mlci"
  justtalk
   D: "Chyba mo�esz."
  juststay
  start apok "mluvi"
   A: "No, dobrze.
       Teraz uwa�aj -
       Chc� przemieni� kamie� w py�!"
  start apok "mlci"
  justtalk
   D: "Ale to potrafi ka�dy nowicjusz!"
  juststay
  start apok "mluvi"
   A: "By� mo�e.
       Lecz nie s�dzisz chyba,
       �e ja jestem jakim� g�upim nowicjuszem!"
  start apok "mlci"
  justtalk
   D: "Ale� sk�d."
  juststay
  start apok "mluvi"
   A: "Widzisz!"
  start apok "mlci"
gplend

block _05 ( beg(0) and not been(_05) and been(_04) )
   TITLE Czy ju� wybra�e� jaki� kamie�?
  justtalk
   D: "Czy ju� wybra�e� jaki� kamie�?"
  juststay
  start apok "mluvi"
   A: "Troch� si� rozejrza�em dooko�a
       i my�l�, �e co� znalaz�em.
       To tamten kamie�."
  start apok "mlci"
  {kouzeln�k uk��e na malinkej k�men opod�l, drak oto�� hlavu,
  {pod�v� se na n�j a ud�l� udivenej ksicht
  walkonplay 100 164 vlevo
  justtalk
   D: "Taki ma�y?"
  juststay
  start apok "mluvi"
   A: "Jest nadzwyczaj ci��ki."
  start apok "mlci"
  walkonplay 104 164 vpravo

  let vim_o_kaminku (1)
gplend

block _06 ( beg(0) and not been(_06) and been(_04) )
   TITLE Czy mog� zobaczy� to twoje zakl�cie?
  justtalk
   D: "Czy mog� zobaczy� to twoje zakl�cie?"
  juststay
  start apok "mluvi"
   A: "My�l�, �e to nie b�dzie mo�liwe.
       O ile..."
  start apok "mlci"
  justtalk
   D: "O ile co?"
  juststay
  start apok "mluvi"
   A: "O ile nie przyst�pimy do
       wsp��zawodnictwa w rzucaniu zakl��.
       nie pami�tam dok�adnie co m�wi�e�.
       lecz mam nadziej�, �e rzucasz zakl�cia?!"
  start apok "mlci"
  justtalk
   D: "Pami�tasz doskonale.
       Ten pomys� wsp��zawodnictwa jest naprawd�
       wspania�y."
  juststay
  start apok "mluvi"
   A: "Ale ja nie chc� traci� czasu dla byle kogo.
       Czy mo�esz udowodni� mi swe kwalifikacje?"
  start apok "mlci"
  justtalk
   D: "Jestem takim samym czarodziejem jak ty.
       Co mam ci udowadnia�?"
   D: "Mo�e powinienem ci co� pokaza�..."
  juststay
  start apok "mluvi"
   A: "Nie ma mowy, znam te n�dzne sztuczki!
       Wiem do czego zmierzasz, ty oszu�cie."
   A: "Nie dam si� z�apa� w twoj� pu�apk�."
  start apok "mlci"

  let prokazal_kvalifikaci (1)
gplend

block _08 ( beg(0) and not been(_08) and been(_07) and been(_07) and not been(_09) )
   TITLE Ale kto b�dzie ocenia� wyniki naszego...
  justtalk
   D: "Ale kto b�dzie ocenia� wyniki naszego
       wsp��zawodnictwa?"
  juststay
  start apok "mluvi"
   A: "Ja."
  start apok "mlci"
  justtalk
   D: "Co?!
       Czy� da si� to zrobi� inaczej?"
  juststay
  start apok "mluvi"
   A: "Dobrze, wy�onimy zwyci��c� poprzez g�osowanie."
  start apok "mlci"
  justtalk
   D: "Taak..."
  juststay
  start apok "mluvi"
   A: "Ja mam jeden g�os i ty te�."
  start apok "mlci"
  justtalk
   D: "Ale ja chc� mie� dwa g�osy."
  juststay
  start apok "mluvi"
   A: "Ja chc� mie� trzy."
  start apok "mlci"
  justtalk
   D: "A ja cztery!"
  juststay
  start apok "mluvi"
   A: "Pi��!"
  start apok "mlci"
  justtalk
   D: "Sze��!"
  juststay
  start apok "mluvi"
   A: "Siedem!"
  start apok "mlci"
  justtalk
   D: "Osiem!"
  juststay
  start apok "mluvi"
   A: "Dziewi��!"
  start apok "mlci"
  justtalk
   D: "Dziesi��!"
  juststay
  justtalk
   D: "Wystarczy.
       Zwyci�zca bierze wszystko."
  juststay
  start apok "mluvi"
   A: "S�usznie.
       Nie mog� ju� poda� wi�kszej liczby."
  start apok "mlci"
gplend

block _11 ( beg(0) and maxline(1) and last(_00a) )
title
  justtalk
   D: "Nic z tego..."
  juststay
  exitdialogue
gplend

block _10 ( beg(0) and not maxline(1) and not (last(_00b) or last(_00a)))
   TITLE A wi�c �egnaj...
  justtalk
   D: "A wi�c �egnaj..."
  juststay
  exitdialogue
gplend
