{s v�trem v hor�ch:}

block _00 ( beg(1) and isactico(0) and not been(_00) )
title
 labels skip
  justtalk
   D: "Panie drzewo."
  juststay
  justtalk
   D: "Panie drzewo!"
  juststay
  justtalk
   D: "To g�upie drzewo jest nieme, czy co?"
  juststay
  goto skip
  justtalk
   D: "Panie drzewo!!"
  juststay
   V: "To t�pe..."
   V: "... g�upie..."
   V: "... zwyrodnia�e.."
   V: "... idiotyczne drzewo..."
 label skip
   V: "... naprawd� nie umie m�wi�.
       Lecz mo�e ty zechcesz
       porozmawia� ze mn�."
  justtalk
   D: "Kim jeste�?"
  juststay
   V: "Jestem Wiatr."
  justtalk
   D: "A ja my�la�em, �e jeste� drzewem."
  juststay
   V: "Nie, ja jestem wiatr!"
  justtalk
   D: "A wi�c! Czy przeszkadza ci ten las?"
  juststay
   V: "Jeszcze si� pytasz?
       Czy nie widzisz, �e jestem tu w potrzasku!"
  justtalk
   D: "Widz� tylko ko�ysz�ce si� ga��zie."
  juststay
   V: "To jest w�a�nie to.
       Pr�buj� si� st�d wydosta�,
       Lecz sam sobie nie poradz�!"
  justtalk
   D: "Zobaczymy, co si� da zrobi�!"
  juststay
   V: "Nie odpowiadaj tak wykr�tnie,
       Z ka�d� chwil� opadam z si�."
   V: "Wynagrodz� ci to."
  justtalk
   D: "To by�oby mo�liwe.
       Ale nie mo�esz oczekiwa�, �e zrobi� to za darmo!"
  juststay
  justtalk
   D: "Zapewniam ci�, �e nie bior� mniej
       ni� 150 za godzin�!"
  juststay
   V: "Mam nadziej�, �e zmienisz zdanie!
       Ufam ci!"
  justtalk
   D: "W takiej sytuacji
       trzeba mie� do mnie du�o zaufania."
  juststay
gplend

block _01 ( beg(1) and isactico(i_prsten) )
title
  icostat off i_prsten
    objstat away stromek_kym
    objstat_on stromek hory
    start stromek "zakladni"
    load hory_jisk "FLI-animace"
    startplay hory_jisk "FLI-animace"
    objstat away hory_jisk
   V: "Dzi�ki!"
  justtalk
   D: "Poszed� sobie."
  juststay
  justtalk
   D: "Nie ma rady!"
  juststay
gplend

block _02 ( maxline(1) and beg(1) and isactico(0) )
title
  justtalk
   D: "Panie wietrze!"
  juststay
   V: "Jestem tu uwi�ziony.
       Wiesz o tym.
       Jak m�g�by� mi pom�c?"
  justtalk
   D: "Sam chcia�bym wiedzie�."
  juststay
gplend
