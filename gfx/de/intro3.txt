block _0 ( beg(1) )
title
{disablespeedtext

load intr_lout1 "kameni"
load intr_lout1 "mluvi"

load intr_lout2 "kameni"
load intr_lout2 "mluvi"
load intr_lout2 "mlci"
load intr_lout2 "vyleza"

{[jsme u komedianta:]
{komediant m�n� hlas pro n�sleduj�c� dv� postavy:}
{m� to b�t pimprlov� divad�lko, no...
{kdy�tak si to upravte dle va�� libov�le!

{jedna:}
start intr_lout1 "mluvi"
KoIn: "Komm aus der Truhe raus, du bist enttarnt,
       du Verr�ter!"
start intr_lout1 "mlci"
{dva:}
startplay intr_lout2 "vyleza"
start intr_lout2 "mluvi"
KoIn: "Ich habe keinen Grund,
       mich in der Truhe zu verstecken."
KoIn: "Denn ich habe nichts zu verbergen,
       sondern DU, Zauberer!"
KoIn: "Ich werde dein wahres Gesicht entlarven!"
start intr_lout2 "mlci"
{jedna:}
start intr_lout1 "mluvi"
KoIn: "Du verbreitest Unfug!
       Denn du bist es, der Duckm�user des K�nigs!"
start intr_lout1 "mlci"
{dva:}
start intr_lout2 "mluvi"
KoIn: "Hah, wenn dem so ist, m�ge ich zu Stein..."

start intr_lout1 "kameni"
startplay intr_lout2 "kameni"

start intr_lout1 "kamennej"

{uprost�ed v�ty p�etr�eno, loutky zkamen�, jedna spadne {komediantovi na nohu,

{komediant u� sv�m hlasem:}
KoIn: "Aua!"

gplend

block _1 ( (1=2) )
title
KoIn: "."
KoIn: "."
KoIn: "."
KoIn: "."
KoIn: "."
gplend
