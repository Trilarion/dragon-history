{Bert se sk�ety:}

block _00 ( atbegin(1) and maxline(1) and not hasbeen(_00) )
title
  justtalk
D: "Meine Herren..."
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Was machst du denn hier?"
  start blby_hl "mlci"
  start blby "bdi"

  justtalk
D: "Ich bin ein Sprachspezialist,
    ich bin gekommen, um die Chronik zu heilen!"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Wir geben sie dir nicht!"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Bl�dmann, Bl�dmann!"
  start blbejsi_hl "mlci"

  justtalk
D: "(Bis jetzt hab ich nichts geh�rt!
    Und das ist deren Gl�ck...)"
  juststay
gplend

block _00a ( atbegin(1) and hasbeen(_00) and ( (varblock(_00a)<5)or(varblock(_00b)>5) ) )
title
 labels nic
  justtalk
D: "Meine Herren..."
  juststay

Iq: "......."
Blb: "------"

  justtalk
D: "Ihr d�mlichen Idioten!"
  juststay

  start blbejsi_hl "mluvi na dr"
Blb: "Was?"
  start blbejsi_hl "mlci"

  if (varblock(_00b)<6) nic
  resetdialoguefrom
 label nic
gplend

block _00b ( atbegin(1) and maxline(1) and hasbeen(_00a) )
title
 labels nic
  justtalk
D: "Hohlk�pfe!"
  juststay

  start blby_hl "mluvi"
Iq: "H�h?"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Hm?"
  start blbejsi_hl "mlci"

  if (varblock(_00b)>0) nic
  resetdialoguefrom
 label nic
gplend

block _01 ( atbegin(0) and not hasbeen(_01) )
title Ich mu� mit der Chronik sprechen!
  justtalk
D: "Ich mu� mit der Chronik sprechen!"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Hah, hah, hah!
     Das m�ssen wir auch!"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Sie will mit keinem sprechen,
      au�er unserem Anf�hrer."
  start blbejsi_hl "mlci"
gplend

block _02 ( atbegin(0) and not hasbeen(_02) and hasbeen(_01) )
title Kann ich sie sehen?
  justtalk
D: "Kann ich sie sehen?"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Vergi� es."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Hey, warum, �hem, nat�rlich kannst du sie sehen?"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "OK, das ist unser Buch.
     Echt spannend."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Ein echter Groschenroman."
  start blbejsi_hl "mlci"

  justtalk
D: "Ich war mir sicher,
    da� Goblins nur Comicb�cher lesen k�nnen!"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Lesen?"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Was, Lesen?"
  start blbejsi_hl "mluvi na bl"
Blb: "Was bedeutet das, 'Lesen'?"
  start blbejsi_hl "mlci"
gplend

block _11 ( atbegin(0) and not hasbeen(_11) and hasbeen(_02) )
title Ihr wisst nicht, was 'lesen' bedeutet?
  justtalk
D: "Ihr wisst nicht, was 'lesen' bedeutet?"
  juststay

  start blbejsi_hl "mluvi na dr"
Blb: "N�, ich wei� das nicht."
{#goblins are fools,they speak like lunatics
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Dummkopf, ich wei�, was das bedeutet."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Dann sag es mir, h�h?"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Dein Holzkopf w�rde das eh nicht kapieren."
Iq: "Merk dir einfach,
     da� Goblins nicht lesen m�ssen!
     Wof�r sind sprechende B�cher sonst gut?"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Wow, er sagt da� das Buch
      sich uns selbst vorliest!"
Blb: "Aber bisher hat sie nichts gesagt."
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Noch ein Tag und sie �ndert ihre Meinung!"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Aber wir sollten es mit dem Foltern
      nicht �bertreiben."
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Halt's Maul, du Idiot!"
  start blby_hl "mlci"
  start blby "bdi"
gplend

block _03 ( atbegin(0) and not hasbeen(_03) and hasbeen(_11) )
title Ihr habt das arme, kleine Buch gefoltert?!
 labels skip
  justtalk
D: "Ihr habt das arme, kleine Buch gefoltert?!"
  juststay

  start blbejsi_hl "mluvi na dr"
Blb: "Nur ein bi�chen.
      Sie ist ziemlich z�h, ein richtiges Mistst�ck.
      Sie ist wie ein Zwerg ohne Z�hne!"
  start blbejsi_hl "mlci"

  goto skip
Vypravec: "[Nette Anspielung mit den Zwerge, oder?
           Es f�ngt an sarkastisch zu werden.]"
 label skip

  justtalk
D: "Ihr Barbaren!
    So behandelt ihr ein Buch?!"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Oh, nein, nein...du, du verstehst uns nicht ganz.
     Wir foltern sie nicht."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Und was machen wir eigentlich?"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Wir versuchen ihr das Sprechen beizubringen!"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Und warum?"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Wegen dir, du Knallkopf."
  start blby_hl "mlci"
  start blby "bdi"
gplend

block _04 ( atbegin(0) and not hasbeen(_04) )
title Was wollt ihr mit einer sprechenden Chronik?
  let vim_o_pohadce (1)

  justtalk
D: "Was wollt ihr mit einer sprechenden Chronik?"
  juststay

  start blbejsi_hl "mluvi na dr"
Blb: "Naja, unserer Anf�hrer will sich
      mit jemanden unterhalten,
      weil ich nicht so der gespr�chige Typ bin."
Blb: "Und ich bin total bl�de!"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Ja, das bist du wirklich.
     Wer war das noch gleich,
     der ein M�rchen
     zum Einschlafen h�ren wollte, h�h?"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Au ja, M�rchen!"
Blb: "M�rchen!    |"
Blb: "M�rchen!"
Blb: "M�rchen!|"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Halt's Maul, du Dummbeutel!"
  start blby_hl "mlci"
  start blby "bdi"
gplend

block _06 ( atbegin(0) and not hasbeen(_06) )
title Wo habt ihr die Chronik her?
  justtalk
D: "Wo habt ihr die Chronik her?"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Wir haben sie immer bei uns."
Iq: "Immer wenn wir schlecht drauf sind,
     erz�hlt sie uns einen Witz."
Iq: "Sie kennt einen echt guten."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na bl"
Blb: "Wir haben uns ein halbes Jahr
      kaputtgelacht, oder?"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Jau.
     Und jeden Abend, am Feuer,
     singt sie 'Besoffener Goblin' mit uns!"
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "oder 'Die Spur des heiteren Toten'."
Blb: "Ich muss immer weinen, wenn ich das sing.
      Ich bin so sensibel."
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Das einzige Problem ist, da� sie schief singt.
     Und sie singt eine Oktave h�her."
Iq: "Da liegt die Schwierigkeit.
     Ihre quitschige Stimme verr�t uns manchmal fast."
Iq: "Aber sie ist nett.
     Wir kennen uns ziemlich gut."
  start blby_hl "mlci"
  start blby "bdi"
gplend

block _07 ( atbegin(0) and not hasbeen(_07) and hasbeen(_06) )
title Ihr habt das Buch gestohlen!
  justtalk
D: "Ihr habt das Buch gestohlen!"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Sieh an, dieser Stinker versucht,
     uns zu beschuldigen."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Haben wir es etwa DIR geklaut, h�h?"
  start blbejsi_hl "mlci"

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Wir haben es von niemanden geklaut!"
  start blby_hl "mlci"
  start blby "bdi"
gplend

block _08 ( atbegin(0) and not hasbeen(_08) and hasbeen(_03) )
title H�rt auf sie zu foltern!
  justtalk
D: "H�rt auf sie zu foltern!"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Vergiss es.
     Welche Ma�nahmen wir einsetzen,
     ist unsere Sache."
  start blby_hl "mlci"
  start blby "bdi"

  start blbejsi_hl "mluvi na dr"
Blb: "Unser Anf�hrer sagt,
      da� unsere Methoden
      den neuesten wissenschaftlichen
      Erkenntnisse folgen."
  start blbejsi_hl "mlci"
gplend

block _09 ( atbegin(0) and not hasbeen(_09) and lastblock(_08) )
title Darf ich den Namen dieser neuen Methode erfahren?
  justtalk
D: "Darf ich den Namen dieser neuen Methode
    erfahren??"
  juststay

  start blby_hl "mluvi"
  start blby "mluvi"
Iq: "Lernen gleich Folter."
{#it is based on typical czech student's excuse that
{#learning is some kind of heavy torture
  start blby_hl "mlci"
  start blby "bdi"
gplend

{block _10 ( atbegin(0) and not hasbeen(_10) and hasbeen(_04) )
{title Copak sk�etov� maj� r�di poh�dky?
{  justtalk
{ D:  "Copak sk�etov� maj� r�di poh�dky?
{      (J� u� jsem z poh�dek na�t�st� vyrostl!)"
{ juststay
{
{ start blbejsi_hl "mluvi na dr"
{ Blb:"J� poh�dky �eru!"
{ start blbejsi_hl "mlci"
{
{ start blby_hl "mluvi"
{ start blby "mluvi"
{ Iq: "Von poh�dky v��n� �ere.
{      Ale to je asi jedinej."
{ start blby_hl "mlci"
{ start blby "bdi"
{gplend
