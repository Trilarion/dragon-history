{vyhl��en� z�kazu poj�d�n� trpasl�k�:}

block _00 ( beg(1) )
title
 labels skip
  load trub "troubi"
  load trub "papir dol"
  load trub "papir nah"
  load hradven_trump "leti"
  startplay trub "troubi"
  startplay trub "troubi"
  start trub "papir dol"

Trubac: "H�rt! H�rt!"

Trubac: "Nach k�niglichem Erlass"

Trubac: "Wird von diesem Momente an"

Trubac: "das niedere und primitive Verhalten"

Trubac: "Zwerge zu t�ten und zu verspeisen"

Trubac: "nicht mehr l�nger toleriert!"

Trubac: "Wir sind stolz zu verk�nden"

Trubac: "da� wir nun auf derselben Stufe"

Trubac: "mit anderen zivilisierten"

Trubac: "L�ndern"

Trubac: "aufgrund dieser gerechten"

Trubac: "und w�rdevollen"

Trubac: "Tat stehen."

  goto skip
Trubac: "Lang lebe der Burgherr, Masterfive!"
 label skip

{zazn� hromov� Ur� jako na Rud�m n�m�st� v Moskv�}
  startplay trub "papir nah"
  startplay trub "troubi"
  startplay trub "troubi"
  startplay hradven_trump "leti"

  let je_tam_trumpeta (1)

{zmizime pajzly u obra, sklenici s utopenci
  objstat away obr_pajzl1
  objstat away obr_pajzl2
  objstat away akvarko

  objstat_on wob_provaz obruvnitr
  objstat_on wob_kolo obruvnitr

{zmizime hrnec a ohen v kuchyni
  objstat away uhliky_hori
  objstat away hrnec

  objstat_on uhliky_chlad kuch

gplend
