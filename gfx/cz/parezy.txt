{dva star� letit� pa�ezy- Strombuk a Stromdub:

block _08 ( maxline(1) and beg(1) and not been(_08) and been(_07) and isobjoff(tajnyvchod) )
title
  justtalk
    D: "Co Stromdubova migr�na?"
  juststay
  start par6 "mluvi"
  dub: "V�bec nepolevuje! Nev�� o n�jak� vhodn� terapii?"
  start par6 "mlci"
  justtalk
    D: "Jenom po�kej, donesu si pilu..."
  juststay
  start par6 "mluvi"
  dub: "Takov� terapie se odm�t�m z��astnit!"
  start par6 "mlci"
  exitdialogue
gplend

block _09 ( maxline(1) and beg(1) and isobjon(tajnyvchod) )
title
  justtalk
    D: "Co va�e rozpraskan� ko�eny?"
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "�le, mysl�m si,
        �e je trochu prot�hnout
        nen� �as od �asu v�bec na �kodu!"
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  exitdialogue
gplend

block _00 ( maxline(1) and beg(1) and not been(_00) )
title
  justtalk
    D: "Dobr� den, pane pa�ez."
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Kdyby t� to zaj�malo, jmenuju se Strombuk."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "A j� Stromdub. M� nesm�� opomenout."
  start par6 "mlci"
  justtalk
    D: "Tak�e jste opravdu dva..."
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Kdo?"
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "My?"
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Ne!"
  buk: "Pokud jsi n�co sly�el nav�c, byla to asi ozv�na."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "Hlup�ku, pokazil's to. Te� byla �ada na m�!"
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Ho h�, brachu, to se m�l��!
        U� deset let to ��k�me p�esn�..."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  justtalk
    D: "Zastavte!
        Tenhle v�� vt�pek nen� v�bec vtipnej!"
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "To je n�m l�to.
        Ostatn�, b�t pa�ezem
        nen� ��dn� velk� z�bava!"
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  justtalk
    D: "Ne��k�m, �e je. J� se jmenuju Bert."
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "T��� m�."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "Kdepak jeho, t��� hlavn� m�!"
  start par6 "mlci"
gplend

block _01 ( maxline(1) and beg(1) and been(_00) and isobjoff(tajnyvchod) )
title
  justtalk
    D: "Mil� pa�ezy..."
  juststay
gplend

block _02 ( beg(0) and not been(_02) and isobjoff(tajnyvchod) )
title Mohli byste mi povypr�v�t n�co o sob�?
  justtalk
    D: "Mohli byste mi povypr�v�t n�co o sob�?"
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Dobr�..."
  buk: "Prvn� rok sv�ho �ivota jsem byl mal�
        sem�nko a l�tal jsem vzduchem."
  buk: "Nev�m, jak dlouho jsem vlastn� l�tal,
        ale utkv�l mi v pam�ti okam�ik,
        kdy jsem poprv� spat�il toto m�sto."
  buk: "Hned jsem v�d�l, �e tady zapust�m ko�eny.
        Ladn� jsem zakrou�il a snesl jsem se k zemi."
  buk: "M�kce jsem dopadl do mechu
        hned vedle Stromduba, kter� tady musel
        nouzov� p�ist�t chv�li p�ede mnou."
  buk: "Ten den byla opravdu hrozn� fujavice,
        vypadalo, �e bude pr�et,
        a on se tady cht�l ukr�t p�ed de�t�m."
  buk: "Nakonec jsme se rozhodli,
        �e tady z�staneme oba."
  buk: "Druh�m rokem u� ze m� byl
        mal� semen��ek a ze Stromduba
        o n�co men��."
  buk: "I kdy� tady p�ist�l
        d��v ne� j�, m� se poda�ilo
        zako�enit o n�jak� ��sek p�ed n�m."
  buk: "Od t� doby Stromdubovi marn� vysv�tluju,
        �e jsem star�� a tud�� i chyt�ej��."
  buk: "Sna�� se m� p�esv�d�it,
        �e tady byl d��v ne� j�.
        To mo�n� jo, ale chyt�ej�� nen�."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "L�e."
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "L�e on.
        T�et�m rokem se toho moc nezm�nilo."
  buk: "�tvrt� rok jsem najednou hodn�
        povyrostl a �pln� jsem Stromduba zast�nil."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "To se st�v�, kdy� je strom v pubert�."
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Neposlouchej ho,
        odmali�ka byl trochu opo�d�nej."
  buk: "P�t�m rokem..."

  {st�le otv�r� hubou; objev� se titulek "Pozd�ji..."}
  {obrazovka m��e setm�t a zase se rozsv�tit

  blackpalette
  fadepaletteplay 0 255 50
  loadpalette "..\PAREZY\PL8.PCX"
  disablespeedtext
  fadepalette 0 255 50
  Vypravec: "O p�r dn� pozd�ji..."
  Vypravec: "O p�r dn� pozd�ji..."
  Vypravec: "O p�r dn� pozd�ji..."
  enablespeedtext

  buk: "...a v p�tist�m t�ic�t�m osm�m roce
        jsem ztrouchniv�l zase o n�co v�c."
  buk: "Pr�v� jsi sly�el cel� m�j �ivot,
        douf�m, �e t� to moc nenudilo."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  {drak m�l mezit�m zav�en� o�i, najednou se probud�
  justtalk
    D: "V�bec ne!
        Kdybych m�l je�t� dal�� t�den �as,
        r�d bych si poslechl i Stromdub�v p��b�h."
  juststay
  start par6 "mluvi"
  dub: "To bylo ten prvn� rok, kdy jsem byl..."
  start par6 "mlci"
  justtalk
    D: "Promi�, ale..."
  juststay
  start par6 "mluvi"
  dub: "...je�t� sem�nko a l�tal jsem vzduchem.
        Nev�m, jak dlouho..."
  start par6 "mlci"
  justtalk
    D: "Te� �as nem�m!"
  juststay
  {chv�li oba ml��}
  start par6 "mluvi"
  dub: "Tebe nezaj�m�, jak to doopravdy v�echno bylo?"
  start par6 "mlci"
  justtalk
    D: "Za dal��ch p�tset t�icet osm let
        si p�ijdu poslechnout, co je nov�ho."
  juststay
  start par6 "mluvi"
  dub: "A p�ijde� ur�it�?"
  start par6 "mlci"
  justtalk
    D: "Slibuju."
  juststay
gplend

block _03 ( beg(0) and not been(_03) and isobjoff(tajnyvchod) )
title Co jste vlastn� za stromy?
  justtalk
    D: "Co jste vlastn� za stromy?"
  juststay
  start par6 "mluvi"
  dub: "J� jsem topol."
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Kec�, je oby�ejn� dub.
        U� odmala z toho m�l ov�em komplexy
        a p��l si b�t vzrostl� topol..."
  buk: "...a� tomu nakonec s�m uv��il."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  justtalk
    D: "A co jsi ty?"
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "J� jsem vzrostl� topol."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
gplend


block _06 ( beg(0) and not been(_06) )
title Vy p�ede mnou n�co taj�te!
  justtalk
    D: "Vy p�ede mnou n�co taj�te!"
  juststay
  start par6 "mluvi"
  dub: "J�? Co nev�m, nepov�m."
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Nebo snad j�? Ho h�!"
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  justtalk
    D: "Taj�te p�ede mnou tajn� vchod!"
  juststay
  start par6 "mluvi"
  dub: "Proto je taky tajn�, chytr�ku."
  start par6 "mlci"
gplend

block _07 ( beg(0) and not been(_07) and been(_06) )
title Ne�lo by ten tajn� vchod trochu odtajnit?
  justtalk
    D: "Ne�lo by ten tajn� vchod trochu odtajnit?"
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "To st���.
        P�ikr�v�me ho sv�mi ko�eny,
        a ty jsou u� lety p�kn� ztuhl�."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "Nejsem sebevrah,
        nejsp�� by se mi v�echny ko�eny
        pol�maly a popraskaly by."
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Na svoje ko�eny ned�me dopustit!
        Kdepak, p�id�l�vat si starosti s tajn�m vchodem!"
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "I te� �pln� jasn� c�t�m v ko�enech,
        jak pomalu ale jist� rozprask�vaj�."
  dub: "Jau!"
  start par6 "mlci"
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "M� nav�c p���ern� svrb� li�ejn�k.
        Uhhh, je to hr�za!"
  buk: "A o ko�enech ani nemluv�."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "Vida, na li�ejn�k jsem j� zapomn�l.
        A t�hle se na m� kdosi rozhodl na�t�pat d��v�.
        Od t� doby trp�m i migr�nou."
  start par6 "mlci"
  justtalk
    D: "Tak hypochondrick� pa�ezy jsem je�t� nepotkal!"
  juststay

  let budu_drbat (1)
gplend

block _04 ( beg(0) and maxline(1) and last(_01) and isobjoff(tajnyvchod) )
title
  justtalk
    D: "...nem�m v�m co ��ct."
    D: "Vy snad mn� ano?"
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Ne."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  start par6 "mluvi"
  dub: "To bylo ten prvn� rok, kdy jsem byl..."
  start par6 "mlci"
  justtalk
    D: "Tak tohle zrovna nechci sly�et!"
  juststay
  exitdialogue
gplend

block _05 ( beg(0) and not last(_01) )
title Zat�m nashledanou.
  justtalk
    D: "Zat�m nashledanou."
  juststay
  start par7 "mluvi"
  start par4 "mluvi"
  start par5 "mluvi"
  buk: "Nashle."
  start par7 "mlci"
  start par4 "mlci"
  start par5 "mlci"
  exitdialogue
gplend

