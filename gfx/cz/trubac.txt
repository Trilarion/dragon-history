{vyhl��en� z�kazu poj�d�n� trpasl�k�:}

block _00 ( beg(1) )
title
 labels skip
  load trub "troubi"
  load trub "papir dol"
  load trub "papir nah"
  load hradven_trump "leti"
  startplay trub "troubi"
  startplay trub "troubi"
  start trub "papir dol"

  Trubac: "T�mto!"

  Trubac: "Se na v�domost d�v�!"

  Trubac: "�e od t�to chv�le!"

  Trubac: "Jest nav�dy zapov�zeno!"

  Trubac: "Takov� primitivn� jedn�n�!"

  Trubac: "Jako jest poj�d�n� trpasl�k�!"

  Trubac: "Jsme r�di!"

  Trubac: "�e se t�mto v�zna�n�m!"

  Trubac: "St�tnick�m!"

  Trubac: "�inem!"

  Trubac: "�ad� i na�e zem�!"

  Trubac: "Po bok zem�!"

  Trubac: "Civilizovan�ch!"

  goto skip
  Trubac: "A� �ije n�� osv�cen� p�n P�nnap�t!"
 label skip

{zazn� hromov� Ur� jako na Rud�m n�m�st� v Moskv�}
  startplay trub "papir nah"
  startplay trub "troubi"
  startplay trub "troubi"
  startplay hradven_trump "leti"

  let je_tam_trumpeta (1)

{zmizime pajzly u obra, sklenici s utopenci
  objstat away obr_pajzl1
  objstat away obr_pajzl2
  objstat away akvarko

  objstat_on wob_provaz obruvnitr
  objstat_on wob_kolo obruvnitr

{zmizime hrnec a ohen v kuchyni
  objstat away uhliky_hori
  objstat away hrnec

  objstat_on uhliky_chlad kuch

gplend
