{s v�trem v hor�ch:}

block _00 ( beg(1) and isactico(0) and not been(_00) )
title
 labels skip
  justtalk
  D: "Pane strom."
  juststay
  justtalk
  D: "Pane strom!"
  juststay
  justtalk
  D: "Ten zaostal� strom snad neum� mluvit!"
  juststay
  goto skip
  justtalk
  D: "Pane strom!!!"
  juststay
  V: "Ten zaostal���..."
  V: "...dementn���..."
  V: "...degenerovan���..."
  V: "...mongoloidn��� strom..."
 label skip
  V: "...v����n� neum��� mluvit,
      ale mo�n��� by sis r���d
      promluvil se mnouuu."
  justtalk
  D: "Kdo jsi?"
  juststay
  V: "V�����tr..."
  justtalk
  D: "A j� myslel, �e jsi strom!"
  juststay
  V: "Kdepak, v���tr!"
  justtalk
  D: "Ach tak!
      A ty n�co proti tomu d�evu m��?"
  juststay
  V: "No �e se pt����,
      copak nevid����, �e jsem uv���zl?"
  justtalk
  D: "Vid�m akor�t kym�cej�c� se d�evo."
  juststay
  V: "Tak t��� bude ono.
      Sna����m se vyprostit,
      ale bez ciz��� pomoci to asi nep���jde!"
  justtalk
  D: "Uvid�me, co se t�m d� d�lat."
  juststay
  V: "Neodpov���dej tak vyh���bav�,
      ka�d���m okam�ikem mi ub���v��� sil."
  V: "Odm�n���m se ti!"
  justtalk
  D: "To by snad �lo,
      zadarmo byste se mnou t��ko po��dil!"
  juststay
  justtalk
  D: "Upozor�uju v�s, �e pod stopades�t na
      hodinu nejdu!"
  juststay
  V: "Douf���m, �e si to je�t� rozmysl����.
      Spol���h���m na tebe!"
  justtalk
  D: "P�i hran� jedn� takov�to hry
      na m� le�� spousta odpov�dnosti."
  juststay
gplend

block _01 ( beg(1) and isactico(i_prsten) )
title
  icostat off i_prsten
    objstat away stromek_kym
    objstat_on stromek hory
    start stromek "zakladni"
    load hory_jisk "FLI-animace"
    startplay hory_jisk "FLI-animace"
    objstat away hory_jisk
  V: "D���ky!"
  justtalk
  D: "A je fu�."
  juststay
  justtalk
  D: "To snad ne!"
  juststay
gplend

block _02 ( maxline(1) and beg(1) and isactico(0) )
title
  justtalk
  D: "Pane v�tr!"
  juststay
  V: "Uv���zl jsem.
      A ty to v�����!
      Co s t�m ud�l����?"
  justtalk
  D: "No to by m� zaj�malo."
  juststay
gplend
