block _00 ( beg(1) )
title
 labels preskocime
{  disablespeedtext
  disablequickhero
  walkon 176 156 vlevo
   Vypravec: "Ale to nie koniec naszej historii,
              ale dopiero początek."
  walkonplay 176 156 vlevo
  load intromutti "mluvi"

  start intromutti "mluvi"
   B: "Tak nie można, Bert!"
   B: "To, że twój ojciec gdzieś się włóczy
       nie znaczy, że możesz robić się
       coraz bardziej nieznośny!"
  start intromutti "mlci"
  justtalk
   D: "Co?"
  juststay
  start intromutti "mluvi"
   B: "Kto narysował babci na pupie wielkie ucho?"
  start intromutti "mlci"
  justtalk
   D: "Cicho, jeszcze nas usłyszy."
  juststay
  start intromutti "mluvi"
   B: "Biedna babcia, teraz wstydzi się pokazać
       ludziom na oczy."
  start intromutti "mlci"
  justtalk
   D: "Przyzwyczai się..."
  juststay
   Vypravec: "Zresztą, nie jest to opowieść o babci!"
  goto preskocime
   Vypravec: "[x-not displayed-x]"
   Vypravec: "[x-not displayed-x]"
 label preskocime
  {bert i matka se chápavě podívají někam nahoru, nebo na
  {sebe, pak jako herci začnou zase zostra:}
  mark
  load intromutti "mluvinormal"
  start intromutti "mluvinormal"
   B: "Pytam cię dalej."
  release
  start intromutti "mlci"
  justtalk
   D: "Dobrze."
  juststay

  start intromutti "mluvi"
   B: "Kto zamienił w kamień kukiełki aktora?"
  start intromutti "mlci"

  justtalk
   D: "Czy cały czas masz mnie na myśli?"
  juststay

  justtalk
   D: "To Ewelina!"
  juststay

  start intromutti "mluvi"
   B: "Ewelina?"
  start intromutti "mlci"

  justtalk
   D: "Moja najlepsza przyjaciółka!"
  juststay
  start intromutti "mluvi"
   B: "Musisz przestać się z nią zadawać!"
   B: "Ona sprowadziła cię na złą drogę!"
  start intromutti "mlci"

  justtalk
   D: "Co masz na myśli?"
  juststay
  walkonplay 224 166 vpravo
  justtalk
   D: "Mam to jaśniej wyrazić?"
   D: "Nie musisz!"
   D: "Miałem dobre zamiary..."
  juststay
gplend

block _01 ( last(_00) )
title
  disablequickhero
  load intromutti "mluvi"

  start intromutti "mluvi"
   B: "Bert, podaj mi drewnianą łyżkę!"
{# in this fairy-tale land the children are not beaten
{# with canes but with wooden spoons
  start intromutti "mlci"
  justtalk
   D: "Nie chcesz mnie zbić, prawda?
       W tej grze miało nie być scen przemocy!"
  juststay
  walkon 184 156 vlevo
  start intromutti "mluvi"
   B: "Początkowo miało nie być!"
  start intromutti "mlci"
  walkonplay 184 156 vlevo
  justtalk
  D: "Tylko bez żadnych wylewających się
      flaków proszę!"
  juststay
  start intromutti "mluvi"
  B: "Nie mogę tego zagwarantować, być
      moźe ręka mi się omsknie..."
  start intromutti "mlci"
  justtalk
   D: "Nie masz wprawy w biciu!"
  juststay
  start intromutti "mluvi"
   B: "Nie martw sie, szybko się uczę!"
  start intromutti "mlci"

  justtalk
   D: "Martwi mnie to...
       Lepiej poczekam na ojca."
  juststay
  start intromutti "mluvi"
   B: "Wolę nie czekać."
  start intromutti "mlci"

  walkonplay 200 156 vpravo
  justtalk
   D: "Można trochę poczekać."
  juststay
  start intromutti "mluvi"
   B: "Dobrze, ale nie za długo."
  start intromutti "mlci"
  load intromutti "mlci5sec"
{čekají asi 5 sekund}
  startplay intromutti "mlci5sec"
  start intromutti "mlci"
  justtalk
   D: "Nie spodziewamy się ujrzeć go zbyt prędko."
  juststay
  walkon 176 156 vlevo
  start intromutti "mluvi"
   B: "Gdzie do diabła jest Herbert?
       Obawiam się, że odszedł już miesiąc temu!"
  start intromutti "mlci"

  walkonplay 176 156 vlevo
  justtalk
   D: "Nie przejmuj się.
       Zobaczysz, sprowadzę go z powrotem!"
   D: "Możemy poczekać z biciem na jego powrót."
   D: "(Mam nadzieję, że do tego czasu
       uda mi się zwędzić drewnianą łyżkę)."
  juststay
  walkon 240 144 vlevo
  start intromutti "mluvi"
   B: "Przypuszczam, że już nie zobacze cię
       w towarzystwie Eweliny!"
  start intromutti "mlci"
  walkonplay 240 144 vlevo
  justtalk
   D: "Nie martw się. Już ja się nią zajmę."
  juststay
  walkonplay 268 144 vpravo
gplend
